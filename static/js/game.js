function Game() {
    this.canvas = document.getElementById('canvas');
    this.context = this.canvas.getContext('2d');
    this.playerId = undefined;

    this.tanks = [];
    this.blocks = [];
    this.bullets = [];
    this.grid = undefined;
    this.gridSize = undefined;
    this.ws = undefined;
    var self = this;
    // self.addEventListener();
    // self.canvas.addEventListener("onkeypress", self.keyPress.bind(self), false);
    document.addEventListener("keydown", self.keyPress.bind(self), false);
    // document.addEventListener("keydown", self.keyPress.bind(self), false);
    this.KilledBotsDiv = document.getElementById('KilledBotsDiv');
    this.KilledPlayersDiv = document.getElementById('KilledPlayersDiv');
    return this;
}

Game.prototype.keyPress = function (event) {
    // console.log(event.keyCode)
    // this.ws.send(event.keyCode);
    var game_data = {game_data: true, key:event.keyCode};
    this.ws.send(JSON.stringify(game_data))
};

Game.prototype.start = function () {
    var self = this;
    self.drawObjects();
    requestAnimationFrame(self.start.bind(self));
};

Game.prototype.drawObjects = function () {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

    if (this.grid) {
        this.grid.draw();
    }

    this.bullets.forEach(function (object) {
        object.draw()
    });
    this.tanks.forEach(function (object) {
        object.draw()
    });
    this.blocks.forEach(function (object) {
        object.draw()
    });
};


Game.prototype.onmessage  = function (data) {
    var self = this;
    if (data.game_data){
        this.game_onmessage(data)
    }
    if (data.init_data) {
        self.canvas.width = data.game_width;
        self.canvas.height = data.game_height;
        self.playerId = data.player_id;
        self.gridSize = data.cell;
        self.grid = new Grid({canvas: self.canvas, size: this.gridSize});
    }
};

Game.prototype.game_onmessage = function (data) {
    var self = this;

    if (data.count_killed_bots || data.count_killed_players){
        self.KilledBotsDiv.innerText = data.count_killed_bots;
        self.KilledPlayersDiv.innerText = data.count_killed_players;
    }

    if (data.tank_data) {
        var tank;
        this.tanks.forEach(function (object) {
            if (object.id === data.id) {
                tank = object;
            }
        });
        if (tank){
            tank.setCenterXCenterY(data.x0, data.y0);
            tank.pistolDirection = data.pistol_direction;
        }
        else{
            var tank_data = {centerX: data.x0, centerY: data.y0, size: data.size, pistolDirection: data.pistol_direction,
                color: data.color, canvas: self.canvas, id:data.id};
            self.tanks.push(new Tank(tank_data))
        }
    }

    if (data.block_data) {
        var block;
        this.blocks.forEach(function (object) {
            if (object.id === data.id) {
                block = object;
            }
        });
        if (!block){
            var block_data = {centerX: data.x0, centerY: data.y0, size: data.size,
                color: data.color, canvas: self.canvas, id: data.id};
            self.blocks.push(new Block(block_data))
        }
    }
    
    if (data.bullet_data) {
        var bullet;
        this.bullets.forEach(function (object) {
            if (object.id === data.id) {
                bullet = object;
            }
        });
        if (bullet){
            bullet.setCenterXCenterY(data.x0, data.y0)
        }
        else{
            var bullet_data = {centerX: data.x0, centerY: data.y0, canvas: self.canvas, id: data.id};
            self.bullets.push(new Bullet(bullet_data))
        }
    }
    
    if (data.remove_data){
        if (data.tank){
            this.tanks.forEach(function (tank, index) {
                if (tank.id === data.id) {
                    self.tanks.splice(index, 1);
                }
            });
        }
        if (data.bullet){
            this.bullets.forEach(function (bullet, index) {
                if (bullet.id === data.id) {
                    self.bullets.splice(index, 1);
                }
            });
        }
    }
};
