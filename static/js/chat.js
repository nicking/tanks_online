function Chat(ws) {
    this.ws = ws;

    this.inputReader = document.getElementById('chat_input');
    this.chatDiv = document.getElementById('chat_div');
    var self = this;
    document.getElementById('chat_button').onclick = function(){
        var inputData = self.inputReader.value;
        if (inputData){
            var message = {chat_data: true, message: inputData};
            self.ws.send(JSON.stringify(message));
        }
        self.inputReader.value = '';
    }
}

Chat.prototype.onmessage = function (data) {
    var self = this;
    if (data.chat_message) {
        while (self.chatDiv.hasChildNodes()) {
            self.chatDiv.removeChild(self.chatDiv.firstChild);
        }
        console.log(data);
        data.chat_message.forEach(
            function (messageData) {
                self.getMessage(messageData.mail, messageData.sender_id)
            }
        );
    }
};

Chat.prototype.getMessage = function (newMessage, id_sender) {
    var dataDiv = document.createElement('div');
    dataDiv.innerText = newMessage + ' | ' + id_sender;
    this.chatDiv.appendChild(dataDiv)
};

// ws = new WebSocket("ws://0.0.0.0:5678");
// var chat = new Chat(ws);
// ws.onmessage = function(event) {
//     var data = JSON.parse(event.data);
//     game.onmessage(data);
//     if (data.chat_message) {
//         console.log('chat_mes');
//         chat.getMessage(data.mail, data.id)
//     }
// };

// <div>
//     <input id="chat_input">
//     <button id="chat_button">Send</button>
// </div>
// <div id="chat_div">
// </div>