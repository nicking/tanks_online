function Tank(args) {
    this.id = args.id;
    this.centerX = args.centerX;
    this.centerY = args.centerY;
    this.pistolDirection = args.pistolDirection;

    this.rectangle = new Rectangle(args); // centerX, centerY, size, color, canvas
}
Tank.prototype.draw = function () {
    this.rectangle.draw();
    var context = this.rectangle.context;
    context.fillStyle = 'black';
    var rect = this.rectangle;
    if (this.pistolDirection === 'up'){
        context.fillRect(this.centerX - rect.size_x/4, this.centerY, rect.size_x/2, -rect.size_y/2);
    }
    if (this.pistolDirection === 'down'){
        context.fillRect(this.centerX - rect.size_x/4, this.centerY, rect.size_x/2, rect.size_y/2);
    }
    if (this.pistolDirection === 'left'){
        context.fillRect(this.centerX, this.centerY - rect.size_y/4, -rect.size_x/2, rect.size_y/2);
    }
    if (this.pistolDirection === 'right'){
        context.fillRect(this.centerX, this.centerY - rect.size_y/4, rect.size_x/2, rect.size_y/2);
    }
};
Tank.prototype.setCenterXCenterY = function (centerX, centerY) {
    this.centerX = centerX;
    this.centerY = centerY;
    this.rectangle.centerX = centerX;
    this.rectangle.centerY = centerY;
};


function Block(args) {
    this.id = args.id;
    this.centerX = args.centerX;
    this.centerY = args.centerY;

    this.rectangle = new Rectangle(args); // centerX, centerY, size, color, canvas
}
Block.prototype.draw = function () {
    this.rectangle.draw();
    
};


function Bullet(args) {
    this.id = args.id;
    this.centerX = args.centerX;
    this.centerY = args.centerY;
    this.circle = new Circle(args);
}
Bullet.prototype.draw = function () {
    this.circle.draw()
};
Bullet.prototype.setCenterXCenterY = function (centerX, centerY) {
    this.centerX = centerX;
    this.centerY = centerY;
    this.circle.centerX = centerX;
    this.circle.centerY = centerY;
};