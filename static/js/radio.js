function Radio(ws) {
    this.radioDiv = document.getElementById('myRadioDiv');
    this.idMutted = false;
    this.muttedDiv = document.getElementById('muttedDiv');
}

Radio.prototype.onmessage = function (data) {
    if (data.radio_data){
        this.setMusic(data);
    }
};

// defaultMuted = true;
Radio.prototype.setMusic = function (data) {

    while (this.radioDiv.hasChildNodes()) {
        this.radioDiv.removeChild(this.radioDiv.lastChild);
    }

    var radioOutput = document.createElement('audio', 'controls', 'autoplay');
    var source = document.createElement('source');
    source.src = data.music_src;
    source.type = 'audio/mpeg';
    radioOutput.appendChild(source);
    // radioOutput.play();
    this.radioDiv.appendChild(radioOutput);

    // document.querySelector('audio').play();

    if (data.current_music_time){
        radioOutput.currentTime = data.current_music_time;
    }
    if (this.idMutted){
        radioOutput.muted = true
    }
};

// <div id="myRadioDiv"> </div>
