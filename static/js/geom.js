function Triangle(args) {
    this.centerX = args.centerX;
    this.centerY= args.centerY;
    this.length = args.length || 20;
    this.color = "#A2322E";
    this.isUp = args.isUp; // TODO WTF args.isUp || true - only true
    this.canvas = args.canvas;
    this.context = args.canvas.getContext('2d');
}

Triangle.prototype.draw = function (text) {
    this.context.fillStyle = this.color;
    this.context.beginPath();
    if (this.isUp === true) {
        this.context.moveTo(this.centerX - this.length / 2, this.centerY + this.length / 2); // leftPoint
        this.context.lineTo(this.centerX + this.length / 2, this.centerY + this.length / 2); // rightPoint
        this.context.lineTo(this.centerX, this.centerY - this.length / 2); // upPoint
    }
    else {
        this.context.moveTo(this.centerX - this.length / 2, this.centerY - this.length / 2); // leftPointUp
        this.context.lineTo(this.centerX + this.length / 2, this.centerY - this.length / 2); // rightPointUp
        this.context.lineTo(this.centerX, this.centerY + this.length / 2); // downPoint
    }
    this.context.closePath();
    this.context.fill();
    
    if (text) {
        //drawText
    }
};


function Rectangle(args) {
    this.centerX = args.centerX || 0;
    this.centerY = args.centerY || 0;
    this.size_x = args.size_x || args.size;
    this.size_y = args.size_y || args.size;
    this.color = args.color || 'black';
    
    this.canvas = args.canvas;
    this.context = args.canvas.getContext('2d');
}

Rectangle.prototype.draw = function (text) {
    var context =  this.context;
    context.fillStyle = this.color;
    context.fillRect(this.centerX - this.size_x/2, this.centerY - this.size_y/2, this.size_x, this.size_y);

    if (text){
        context.fillStyle = "white";
        context.strokeStyle = "white";
        context.font = "italic 11pt Arial";
        context.fillText(text, this.centerX-8, this.centerY+4);
    }
};


function Circle(args) {
    this.centerX = args.centerX || 0;
    this.centerY = args.centerY || 0;
    this.radius = args.radius || 4;
    this.color = args.color || 'red';
    this.lineWidth = 2;
    this.id = args.id;
    this.canvas = args.canvas;
    this.context = args.canvas.getContext('2d');
}

Circle.prototype.draw = function (text) {
    var context = this.context;
    context.fillStyle = this.color;
    context.beginPath();
    context.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI, false);
    context.fillStyle = this.color;
    context.fill();
    context.lineWidth = this.lineWidth;
    context.strokeStyle = this.color;
    context.stroke();
    if (text){ //drawText
    }
};


function Grid(args) {
    this.size = args.size || 32;
    this.color = 'gray';
    this.canvas = args.canvas;
    this.context = args.canvas.getContext('2d');
    this.lineWidth = 1;

    if (this.canvas.width % this.size && this.canvas.height % this.size) {
        alert('Ohhhh, canvas.width % grip.size && this.canvas.height % grip.size');
    }
}

Grid.prototype.draw = function () {
    var canvasWidth = this.canvas.width;
    var canvasHeight = this.canvas.height;
    var context = this.context;
    context.lineWidth = this.lineWidth;
    context.strokeStyle = this.color;

    for (var indexForWidth=0; indexForWidth<=canvasWidth/this.size; indexForWidth++){ // vertical
        context.beginPath();
        context.moveTo(indexForWidth * this.size, 0);
        context.lineTo(indexForWidth * this.size, canvasHeight);
        context.stroke();
    }
    for (var indexForHeight=0; indexForHeight<=canvasHeight/this.size; indexForHeight++) { // horisont TODO уточнить англ
        context.beginPath();
        context.moveTo(0, indexForHeight * this.size);
        context.lineTo(canvasWidth, indexForHeight * this.size);
        context.stroke();
    }
};