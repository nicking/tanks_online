import asyncio

from server.server import Server

server = Server(port=9999)

asyncio.get_event_loop().create_task(server.background_game())
asyncio.get_event_loop().run_until_complete(server.server_socket)
asyncio.get_event_loop().create_task(server.background_radio())

asyncio.get_event_loop().run_forever()
