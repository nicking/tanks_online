# -*- coding:utf-8 -*-
import random
import uuid


class Bullet(object):
    def __init__(self, x0, y0, power, direction, id_player, belong_bot, player):
        self.id = uuid.uuid4().hex
        self.y0 = int(y0)
        self.x0 = int(x0)
        self.power = power
        self.direction = direction
        self.speed = 10
        self.radius = 5
        self.death = False
        self.id_player = id_player
        self.belong_bot = belong_bot
        self.player = player

    __slots__ = (
        "id",
        "y0",
        "x0",
        "death",
        "power",
        "speed",
        "radius",
        "direction",
        "id_player",
        "belong_bot",
        "player",
    )

    async def loop(self):
        if self.direction == "up":
            self.y0 -= self.speed
        if self.direction == "down":
            self.y0 += self.speed
        if self.direction == "right":
            self.x0 += self.speed
        if self.direction == "left":
            self.x0 -= self.speed

    async def entering_blocks(self, blocks: list["Block"]):
        for block in blocks:
            if abs(self.x0 - block.x0) <= abs(self.radius / 2 + block.size / 2) and abs(
                self.y0 - block.y0
            ) <= abs(self.radius / 2 + block.size / 2):
                if hasattr(block, "healths"):
                    block.healths -= self.power
                self.death = True

    async def entering_players(self, enemy_players):  # попадание в танк,
        for enemy in enemy_players:
            if abs(self.x0 - enemy.x0) <= abs(self.radius / 2 + enemy.size / 2) and abs(
                self.y0 - enemy.y0
            ) <= abs(self.radius / 2 + enemy.size / 2):
                enemy.healths -= self.power
                self.death = True
                return "Killed"


class Pistol(object):
    directions = ("up", "down", "right", "left")

    def __init__(self):
        self.patron = 8
        self.power = 5
        self.direction = random.choice(self.directions)

    __slots__ = ("patron", "power", "direction")

    def shoot(self, player):
        if self.patron == 0:
            self.reload()

        bullet = Bullet(
            power=self.power,
            x0=player.x0,
            y0=player.y0,
            direction=self.direction,
            id_player=player.id,
            belong_bot=player.is_bot,
            player=player,
        )
        self.patron = self.patron - 1
        return bullet

    def reload(self):
        self.patron = 8


class Player(object):

    def __init__(self, x0, y0, color, size, game, speed=3, healths=5, websocket=None):
        self.id = (
            str(id(websocket))[-5:] if websocket else str(random.randint(1, 99999999))
        )
        self.x0, self.y0 = game.find_center_cell_for(x0, y0)
        # x1, y1 текущая позиция середины
        self.x1 = self.x0
        self.y1 = self.y0
        self.game = game
        self.color = color
        self.size = size
        self.reload_patron_time = 0
        self.reload_time = 10
        self.__speed = self.set_speed(speed)
        self.healths = healths
        self.pistol = Pistol()
        self.stopped = False
        self.is_bot = False if not hasattr(self, "is_bot") else True
        self.count_killed_bots = 0
        self.count_killed_players = 0
        self.websocket = websocket

    __slots__ = (
        "id",
        "x0",
        "x1",
        "y0",
        "y1",
        "game",
        "color",
        "size",
        "reload_patron_time",
        "reload_time",
        "__speed",
        "healths",
        "pistol",
        "stopped",
        "is_bot",
        "count_killed_bots",
        "count_killed_players",
        "websocket",
    )

    def set_speed(self, speed=None):
        if speed:
            self.__speed = (
                speed if (speed < self.game.cell / 2) else (self.game.cell / 2 - 1)
            )
        return self.__speed

    async def shoot(self):
        if self.reload_patron_time <= 0:
            bullet = self.pistol.shoot(player=self)
            self.game.bullets.append(bullet)
            self.reload_patron_time = self.reload_time

    def move(self, dx=0, dy=0):
        if self.x0 == self.x1 and self.y0 == self.y1:
            self.x1 = self.x1 + dx
            self.y1 = self.y1 + dy

    def run(self):
        if self.x1 > self.x0 and self.x1 <= self.game.game_width - (self.game.cell / 2):
            self.x0 += self.__speed
            if self.x0 > self.x1:
                self.x0 = self.x1

        elif self.x1 < self.x0 and self.x1 >= 0 + (self.game.cell / 2):
            self.x0 -= self.__speed
            if self.x0 < self.x1:
                self.x0 = self.x1

        if self.y1 > self.y0 and self.y1 <= self.game.game_height - (
            self.game.cell / 2
        ):
            self.y0 += self.__speed
            if self.y0 > self.y1:
                self.y0 = self.y1

        elif self.y1 < self.y0 and self.y1 >= 0 + (self.game.cell / 2):
            self.y0 -= self.__speed
            if self.y0 < self.y1:
                self.y0 = self.y1

    def valid_x1_y1(self, blocks: list["Block"]):
        # проверка на границу игры
        if self.x1 < 0 + (self.game.cell / 2):
            self.x1 = self.game.cell / 2
        if self.x1 > self.game.game_width - (self.game.cell / 2):
            self.x1 = self.game.game_width - (self.game.cell / 2)
        if self.y1 < 0 + (self.game.cell / 2):
            self.y1 = +(self.game.cell / 2)
        if self.y1 > self.game.game_height - (self.game.cell / 2):
            self.y1 = self.game.game_height - (self.game.cell / 2)

        # проверка на блоки
        for block in blocks:
            # max_distance_to_block = self.size/2 + block.size/2
            max_distance_to_block = block.size / 2
            is_small_x_distance = max_distance_to_block > abs(self.x1 - block.x0)
            is_small_y_distance = max_distance_to_block > abs(self.y1 - block.y0)
            if is_small_x_distance and is_small_y_distance:
                self.stop()
                self.stopped = True

        # 50(25 - 75)

    def stop(self):
        self.x1, self.y1 = self.game.find_center_cell_for(self.x0, self.y0)

    async def loop(self):
        self.run()
        self.valid_x1_y1(self.game.blocks)

        if self.reload_patron_time:
            self.reload_patron_time -= 1


class Block(object):  # преграда
    def __init__(self, x0, y0, size, game):
        self.id = uuid.uuid4().hex
        self.x0, self.y0 = game.find_center_cell_for(x0, y0)
        self.size = size  # size
        self.game = game
        self.color = "black"

    __slots__ = ["id", "x0", "y0", "size", "game", "color"]
