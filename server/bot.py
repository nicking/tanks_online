import copy
import random

from .models import Player


class Bot(Player):

    def __init__(self, x0, y0, color, size, game, speed=1, healths=1):
        super(Bot, self).__init__(
            x0=x0,
            y0=y0,
            color=color,
            size=size,
            game=game,
            speed=speed,
            healths=healths,
        )
        self.directions = ["up", "down", "right", "left"]
        self.direction = random.choice(self.directions)
        self.pistol.direction = self.direction
        self.count_steps = 0
        self.is_bot = True

    async def bot_loop(self):
        await self.move_for_direction()
        await self.get_bot_valid_x1_y1()

        if self.stopped == True:
            await self.get_new_bot_and_pistol_direction()
            self.stopped = False

        if self.count_steps > 1:
            self.count_steps -= 1
        else:
            await self.get_new_bot_and_pistol_direction()
            self.count_steps = random.randint(30, 60)

        await self.shoot()
        await self.loop()

    async def get_bot_valid_x1_y1(self):
        if self.direction == "left" and self.x1 < self.game.cell / 2:
            await self.get_new_bot_and_pistol_direction()
        if (
            self.direction == "right"
            and self.x1 >= self.game.game_width - self.game.cell
        ):
            await self.get_new_bot_and_pistol_direction()
        if self.direction == "up" and self.y1 <= self.game.cell:
            await self.get_new_bot_and_pistol_direction()
        if (
            self.direction == "down"
            and self.y1 >= self.game.game_height - self.game.cell
        ):
            await self.get_new_bot_and_pistol_direction()

    async def get_new_bot_and_pistol_direction(self):
        directions_without_current = copy.deepcopy(self.directions)
        directions_without_current.remove(self.direction)
        self.direction = random.choice(directions_without_current)
        self.pistol.direction = self.direction

    async def move_for_direction(self):
        if self.direction == "up":
            self.move(dy=-self.game.cell)
        if self.direction == "down":
            self.move(dy=self.game.cell)
        if self.direction == "right":
            self.move(dx=self.game.cell)
        if self.direction == "left":
            self.move(dx=-self.game.cell)
