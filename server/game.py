# -*- coding:utf-8 -*-
import asyncio
import datetime
import math
import typing
from random import choice

from .bot import Bot
from .models import Block, Player

if typing.TYPE_CHECKING:
    from .server import Server


class Game(object):
    def __init__(
        self,
        level: list[list[int]],
        game_height: int,
        game_width: int,
        cell: int,
        server: "Server",
        count_bots_on_map=3,
        count_bots=10,
    ):

        self.server = server
        self.level = level
        self.game_height = game_height
        self.game_width = game_width
        self.cell = cell  # размер ячейки (квадратика)

        self.player_places = []
        self.players: list[Player] = []

        self.bots: list[Bot] = []
        self.count_bots = count_bots
        self.count_bots_on_map = count_bots_on_map
        self.bot_places = []  # список мест, от куда появляются боты

        self.blocks: list[Block] = []
        self.colors = ["blue", "red", "green", "purple", "orange"]

        self.bullets = []
        self.read_map()

        if game_width % cell != 0 or game_height % cell != 0:
            raise Exception("игра требует кратности ячейки полю")

    async def add_player(self, websocket) -> Player:
        x0, y0 = choice(self.player_places)
        player_data = {
            "x0": x0 * self.cell,
            "y0": y0 * self.cell,
            "websocket": websocket,
            "color": "green",
            "size": 40,
            "game": self,
            "speed": 5,
        }
        player = Player(**player_data)
        self.players.append(player)
        return player

    async def kill_without_healths(self):
        for bot in self.bots:
            if bot.healths <= 0:
                self.bots.remove(bot)
                remove_tank_data = {"remove_data": True, "tank": True, "id": bot.id}
                await self.send_data_for_players(remove_tank_data)

        for player in self.players:
            if player.healths <= 0:
                remove_tank_data = {"remove_data": True, "tank": True, "id": player.id}
                await self.send_data_for_players(remove_tank_data)
                self.players.remove(player)

    def find_center_cell_for(self, x: int, y: int):
        center_cell_x = math.floor(x / self.cell) * self.cell + self.cell / 2
        center_cell_y = math.floor(y / self.cell) * self.cell + self.cell / 2

        if x > self.game_width:
            raise Exception("x > game.width")
        if y > self.game_height:
            raise Exception("x > game.width")

        return center_cell_x, center_cell_y

    def read_map(self):
        for index_y, y_list_obj in enumerate(self.level):
            for index_x, x_obj in enumerate(y_list_obj):
                if x_obj == 1:  # хорошо бы вынести в какую-нибудь переменную
                    self.player_places.append([index_x, index_y])
                if x_obj == 2:
                    self.bot_places.append([index_x, index_y])
                if x_obj == 3:
                    self.blocks.append(
                        Block(
                            index_x * self.cell,
                            index_y * self.cell,
                            self.cell,
                            game=self,
                        )
                    )

    async def append_bots(self):
        if len(self.bots) < self.count_bots_on_map and self.count_bots > 0:
            # self.count_bots -= 1 TODO
            index_x, index_y = choice(self.bot_places)
            self.bots.append(
                Bot(
                    index_x * self.cell,
                    index_y * self.cell,
                    "blue",
                    40,
                    speed=3,
                    game=self,
                )
            )

    async def send_full_data(self):
        for player in self.players:
            await self.send_tank_data(player)

        for bot in self.bots:
            await self.send_tank_data(bot)

        for block in self.blocks:
            block_data = {
                "block_data": True,
                "x0": block.x0,
                "y0": block.y0,
                "id": block.id,
                "size": block.size,
                "color": block.color,
            }
            await self.send_data_for_players(block_data)

        for bullet in self.bullets:
            bullet_data = {
                "bullet_data": True,
                "x0": bullet.x0,
                "y0": bullet.y0,
                "id": bullet.id,
                "direction": bullet.direction,
            }
            await self.send_data_for_players(bullet_data)

    async def send_tank_data(self, tank: Player):
        player_data = {
            "id": tank.id,
            "x0": tank.x0,
            "x1": tank.x1,
            "y0": tank.y0,
            "y1": tank.y1,
            "color": tank.color,
            "size": tank.size,
            "healths": tank.healths,
            "pistol": {
                "patron": tank.pistol.patron,
                "power": tank.pistol.power,
                "direction": tank.pistol.direction,
            },
            "stopped": tank.stopped,
            "is_bot": tank.is_bot,
            "count_killed_bots": tank.count_killed_bots,
            "count_killed_players": tank.count_killed_players,
            "pistol_direction": tank.pistol.direction,
            "tank_data": True,
        }
        # player_data.pop('game')
        # player_data.pop('websocket')
        # player_data['pistol_direction'] = tank.pistol.direction
        # player_data['tank_data'] = True
        await self.send_data_for_players(player_data)

    async def send_data_for_players(self, data):
        data["game_data"] = True
        await self.server.send_data_websockets(data)

    async def bullets_death(self):
        for bullet in self.bullets:
            if bullet.death:
                self.bullets.remove(bullet)
                bullet_death_data = {
                    "remove_data": True,
                    "bullet": True,
                    "id": bullet.id,
                }
                await self.send_data_for_players(bullet_death_data)
                continue

            if bullet:
                if (
                    bullet.x0 < 0
                    or bullet.x0 > self.game_width
                    or bullet.y0 < 0
                    or bullet.y0 > self.game_height
                ):
                    self.bullets.remove(bullet)
                    bullet_death_data = {
                        "remove_data": True,
                        "bullet": True,
                        "id": bullet.id,
                    }
                    await self.send_data_for_players(bullet_death_data)

    async def bullet_in_players_and_blocks(self):
        for bullet in self.bullets:
            if bullet.belong_bot:
                await bullet.entering_players(self.players)
            else:
                if await bullet.entering_players(self.bots):
                    bullet.player.count_killed_bots += 1

                players_without_bullet_player = [
                    player
                    for player in self.players
                    if not player.id == bullet.id_player
                ]
                if await bullet.entering_players(players_without_bullet_player):
                    bullet.player.count_killed_players += 1

            await bullet.entering_blocks(self.blocks)

    async def bullets_loop(self):
        await self.bullets_death()
        for bullet in self.bullets:
            await bullet.loop()
        await self.bullet_in_players_and_blocks()

    async def send_to_players_killed_bots_and_players(self):
        for player in self.players:
            player_data = {
                "game_data": True,
                "count_killed_bots": player.count_killed_bots,
                "count_killed_players": player.count_killed_players,
            }
            await self.server.send_data_websocket(
                data=player_data, websocket=player.websocket
            )

    async def onmessage(self, websocket, player, recv_data):
        if recv_data.get("key"):
            if player in self.players:
                await self.move_player(player, recv_data["key"])

    async def move_player(self, player: Player, key: str):
        key = str(key)
        if key == "65":
            player.move(dx=-self.cell)
            player.pistol.direction = "left"
        if key == "68":
            player.move(dx=+self.cell)
            player.pistol.direction = "right"
        if key == "87":
            player.move(dy=-self.cell)
            player.pistol.direction = "up"
        if key == "83":
            player.move(dy=+self.cell)
            player.pistol.direction = "down"
        if key == "32":
            await player.shoot()

    async def main(self):
        while True:
            fps = 30
            tick_time = 1.0 / fps
            start_loop_point = datetime.datetime.now()

            await self.append_bots()

            for player in self.players:
                await player.loop()

            for bot in self.bots:
                await bot.bot_loop()

            await self.kill_without_healths()
            await self.bullets_loop()
            await self.send_full_data()
            # await self.send_to_players_killed_bots_and_players()

            end_loop_point = datetime.datetime.now()
            time_for_loop = (end_loop_point - start_loop_point).total_seconds()
            if time_for_loop > tick_time:
                print("frps проседает {}".format(time_for_loop - tick_time))
                asyncio_sleep = 0.01
            else:
                asyncio_sleep = tick_time - time_for_loop
            await asyncio.sleep(asyncio_sleep)
