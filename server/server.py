import ujson
import websockets

from .chat import Chat
from .game import Game
from .radio import Radio


class Server(object):
    def __init__(self, port):
        self.game = self.create_game()

        print(f"Start websockets server {port=}")
        server_socket = websockets.serve(
            self.handler,
            "0.0.0.0",
            port,
            extra_headers={
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Allow-Methods": "*",
                "Allow": "*",
            },
        )
        self.server_socket = server_socket
        self.handlers = []

        self.chat = Chat(self)
        self.radio = Radio(self)

    async def handler(self, websocket, _):
        player = await self.game.add_player(websocket)
        print(f"added new {player.id=}")
        self.handlers.append(websocket)
        await self.send_initial_game_data(websocket, player)
        await self.radio.send_current_music_data_ws(websocket)
        await self.chat.send_messages_data()
        await self.chat.add_message(
            player_id="system", message=f"💚 added new player_id={player.id}",
        )

        while True:
            try:
                recv_data = await websocket.recv()
                recv_data = ujson.loads(recv_data)

                if recv_data.get("chat_data"):
                    await self.chat.onmessage(player.id, recv_data)

                if recv_data.get("game_data"):
                    await self.game.onmessage(websocket, player, recv_data)

                if not player in self.game.players:
                    player = await self.game.add_player(websocket)

            except Exception as e:
                print("Disconnected", e)
                self.handlers.remove(websocket)
                if player in self.game.players:
                    self.game.players.remove(player)
                    await self.chat.add_message(
                        player_id="system",
                        message=f"❤️ player_id={player.id} disconnected",
                    )
                break

    async def send_data_websockets(self, data):
        data = ujson.dumps(data)
        for handl_websocket in self.handlers:
            try:
                await handl_websocket.send(data)
            except Exception:
                print(
                    "скорее всего хендлер уже отключен, а ему пытаются послать данные"
                )
                continue

    async def send_data_websocket(self, data, websocket):
        if not isinstance(data, str):
            data = ujson.dumps(data)
        websocket.send(data)

    async def background_game(self):
        await self.game.main()

    async def background_radio(self):
        await self.radio.main()

    async def send_initial_game_data(self, handl_websocket, player):
        init_data = {
            "init_data": True,
            "game_data": True,
            "game_height": self.game.game_height,
            "game_width": self.game.game_width,
            "cell": self.game.cell,
            "player_id": player.id,
        }
        init_data = ujson.dumps(init_data)
        await handl_websocket.send(init_data)

    def create_game(self):
        level = [
            [1, 0, 0, 3, 0, 0, 2, 0, 0, 3, 1, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0],
            [3, 0, 0, 3, 3, 0, 0, 0, 3, 0, 0, 0, 3, 0],
            [2, 0, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 3, 0, 3],
            [3, 2, 0, 0, 0, 0, 3, 0, 0, 3, 3, 3, 0, 3],
            [0, 0, 0, 0, 0, 0, 3, 0, 0, 3, 0, 3, 0, 3],
            [0, 2, 0, 0, 0, 3, 3, 0, 0, 2, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0],
        ]  # 1- ИГРОК, 2 - бот, 3- БЛОК 0 -ПУСТОТА

        new_game = Game(
            level=level, game_height=500, game_width=700, cell=50, server=self
        )
        return new_game
