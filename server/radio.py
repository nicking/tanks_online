import asyncio
import datetime
import random

import ujson


class Radio(object):
    def __init__(self, server):
        self.server = server
        self.musics = self.get_musics()
        self.current_index_music = 0

        self.music_time_start = datetime.datetime.now() + datetime.timedelta(seconds=1)
        self.music_time_end = datetime.datetime.now() + datetime.timedelta(seconds=1)
        self.current_music = self.musics[self.current_index_music]["music"]

    async def main(self):
        while True:
            if datetime.datetime.now() > self.music_time_end:
                await self.set_music()
            await asyncio.sleep(1)

    def get_musics(self):
        musics = [
            {"music": "audio/Scorpions-Humanity.mp3", "duration": 320},
            {"music": "audio/NautilusKrilya.mp3", "duration": 207},
            {"music": "audio/hello.mp3", "duration": 235},
            {"music": "audio/ff7.mp3", "duration": 273},
            {"music": "audio/wept.mp3", "duration": 380},
        ]
        if not musics:
            raise Exception("Добавь музыку в список")
        random.shuffle(musics)
        return musics

    async def set_music(self):
        if self.current_index_music >= len(self.musics) - 1:
            self.current_index_music = 0
        else:
            self.current_index_music += 1

        current_mus_data = self.musics[self.current_index_music]
        self.current_music = current_mus_data["music"]
        self.music_time_start = datetime.datetime.now()
        self.music_time_end = datetime.datetime.now() + datetime.timedelta(
            seconds=current_mus_data["duration"]
        )

        await self.send_current_music_data()

    async def send_current_music_data(self):
        await self.server.send_data_websockets(
            {"radio_data": True, "music_src": self.current_music}
        )

    async def send_current_music_data_ws(self, websocket):
        current_music_time = (
            datetime.datetime.now() - self.music_time_start
        ).total_seconds()
        current_music_time -= current_music_time % 1
        await websocket.send(
            ujson.dumps(
                {
                    "radio_data": True,
                    "music_src": self.current_music,
                    "current_music_time": current_music_time,
                }
            )
        )
