class Chat(object):
    def __init__(self, server):
        self.server = server
        self.messages = []  # [{chat_message: 132; sender_id:123}, {}]
        self.max_count_messages = 6

    async def onmessage(self, player_id: str, recv_data: dict):
        message = recv_data.get("message", "")
        if message:
            await self.add_message(player_id, message)

    async def add_message(self, player_id: str, message: str):
        self.messages.append({"mail": message, "sender_id": player_id})
        if len(self.messages) > self.max_count_messages:
            self.messages = self.messages[
                len(self.messages) - self.max_count_messages :
            ]
        await self.send_messages_data()

    async def send_messages_data(self):
        messages_data = {"chat_message": self.messages}
        await self.server.send_data_websockets(messages_data)
