# Всем привет! 

#### Это Петр! Разработчик. Сын маминой подруги
![petr.png](petr.png)


![tanks_screenshot.png](tanks_screenshot.png)


# Архитектура проекта

![arch.png](arch.png)


## Push docker

    docker build . -t gigaconf-2024-tanks-online.cr.cloud.ru/tanks-online-front:latest -f Dockerfile.front --platform=linux/amd64
    docker build . -t gigaconf-2024-tanks-online.cr.cloud.ru/tanks-online-back:latest -f Dockerfile.back --platform=linux/amd64
    
    docker push gigaconf-2024-tanks-online.cr.cloud.ru/tanks-online-back:latest  # port:9999
    docker push gigaconf-2024-tanks-online.cr.cloud.ru/tanks-online-front:latest # port:8080

## Полезные ссылки
- Console Cloud.ru \
  https://console.cloud.ru/
- Artifact Registry \
  https://console.cloud.ru/projects/2a6eec92-9ec8-4bc4-81a7-b9646392d2f6/spa/container-registry/registries?customerId=3822d677-d697-412a-a2b9-d56cfb420306
- Container Apps \
  https://console.cloud.ru/projects/2a6eec92-9ec8-4bc4-81a7-b9646392d2f6/spa/serverless-containers/containers?createdContainerApp=true&customerId=3822d677-d697-412a-a2b9-d56cfb420306
