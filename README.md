## Online Tanks game

![tanks_screenshot.png](tanks_screenshot.png)

#### Two parts: back (websockets server) and fronted (html + js)

## Docker local running
    make run


## Local Development

    brew install pyenv # or another installing pyenv
    pyenv install 3.12
    python3.12 -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt from project folder
    python app.py
